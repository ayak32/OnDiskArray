//
// Created by axel on 5/8/18.
//

#include "DiskArray.h"



#include <string>

template <class type>
OnDiskArray<type>::OnDiskArray(std::string FileName, int MaxValues)
{
    max_values = MaxValues;
    file_out.open(FileName, std::ios::in | std::ios::out | std::ios::binary);
    file_in.open(FileName, std::ios::out | std::ios::in | std::ios::binary);
}
template<class type>
void OnDiskArray<type>::Set(int indx, type val)
{
    file_in.seekp(indx);
    file_in << val << std::endl;

}

//version 2
template <class type>
type OnDiskArray<type>::Get(int indx)
{
    file_out.seekg(0);
    int index = 0;
    std::string line;
    while ( std::getline (file_out,line) )
    {
        if(index < indx){
            break;
        }
    }
    type value = type(line);
    return value;
}

template <class type>
OnDiskArray<type>::~OnDiskArray()
{
    file_out.close();
    file_in.close();
}
