//
// Created by axel on 5/8/18.
//

#ifndef ONDISKARRAY_DISKARRAY_H
#define ONDISKARRAY_DISKARRAY_H


#include  <string>
#include  <fstream>
#include <iostream>

template<typename type>
class OnDiskArray
{
private:
    int max_values;
    std::ofstream file_in;
    std::ifstream file_out;
public:

    OnDiskArray(std::string FileName, int MaxValues);

    void Set(int index, type value);
    type Get(int index);

    ~OnDiskArray();
};


#endif //ONDISKARRAY_DISKARRAY_H
