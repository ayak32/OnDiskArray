
#include "DiskArray.cpp"
#include <string>

int main() {

	std::string filename = "/home/axel/notes/daniel/OnDiskArray/OnDiskArray/MyThingies.dat";
	OnDiskArray<std::string> Thingies(filename, 10);

	std::string T1, T2, T3;
	T1 = std::string("hola");

	Thingies.Set(1, T1);

	T2 = Thingies.Get(5);

	return 0;
}